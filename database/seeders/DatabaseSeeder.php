<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use App\Models\Publication;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory(10)->create();
        $Adam = User::factory()->create([
            'name' => 'Adam Psikuta',
            'email' => 'test@example.com',
        ]);
        $Warcin = User::factory()->create([
            'name' => 'Warcin Maszak',
            'email' => 'pogongola@gmail.com',
        ]);
        Publication::create([
            'title' => 'Ubisoft dodaje kolejna bezsensowna postac!',
            'content' => 'Ten nowy gosc to ma lud to jest totalny bezsens cn. Jest mega op i nie da sie przepalic sciany wtf!!',
            'author_id' => $Adam->id,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        Publication::create([
            'title' => 'Dwoch 10-cio latkow sprzedalo radiowoz',
            'content' => 'Do sytuacji doszlo w Rzeszowie, Adam i Marek najpierw radiowoz ukradli a nastepnie wystawili go na aplikacje "OTOMOTO". Zgarneli oni jedynie 5400zl. Za oszustwo podatkowe grozi im do 12 lat pozbawienia komputera',
            'author_id' => $Adam -> id,

        ]);
        Publication::create([
            'title' => 'Puchar polski dla Pogoni',
            'content' => 'Pogon wygrala swoje pierwsze trofeum w XX wieku. `Rozpiera mnie duma` komentuje to najstarszy kibic pogoni - 9 letni Piotrus.',
            'author_id' => $Warcin -> id,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        Publication::create([
            'title' => 'Kibice lecha to brudne malpy i szowinisci',
            'content' => 'Kibice lecha sa niekulturalni, niewychowani, niemyci, nieinteligetni, oraz inne epitety ktorych nie znam. A o kibolach nie bede nawet nic tu pisal...',
            'author_id' => $Warcin -> id,
            'created_at' => date("Y-m-d H:i:s")
        ]);

        


    }
}

@props(['quote', 'hero', 'role', 'img'])
<figure  class="w-72 rounded-xl bg-white duration-75 hover:duration-75 hover:shadow-2xl hover:scale-110">
    <img src="{{ $img }}"  class="w-72" alt="author">
    <div class="bg-sky-900  text-amber-400 h-64 rounded-b-xl">
        <blockquote>{{ $quote }}</blockquote>
        <figcaption>
            <p class="text-right text-xl pr-10 pl-10">{{ $hero }}</p>
            <p class="text-right pr-10 pl-10">{{ $role }}</p>
        </figcaption>
    </div>
</figure>
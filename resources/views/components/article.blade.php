@props(['title', 'date', 'author'])
<figure>
<div class="article">
    <h2>{{ $title }}</h2>
    <p>Published on {{ $date }} by {{ $author }}</p>
</div>
</figure>
@extends('layouts.main')

@section('content')


<div class="tracking-widest uppercase text-center text-5xl">
    <h1>Website done in Laravel!</h1>
    <p>It's {{$date}}</p>
</div>


<div class="flex place-content-around">
    <div class="border-2 p-2 w-1/3 mt-5 " style="background-color: #02919e">
        <h1 class="text-xl font-bold pb-1"> Ostatnia Publikacja: </h1>
        <p class="font-bold italic"> {{$post -> title}} </p>
        <p> {{$post -> content}} </p>
        <p class="text-gray-600 text-right pt-2"> Tworca: {{$post -> author -> name}} </p>
    </div>
</div>
@endsection
@extends('layouts.main')

@section('content')
<h1 class="text-4xl uppercase font-bold my-5">Lepsze Publikacje</h1>

<div class="flex flex-col items-start">
    @foreach ($publications as $id => $publication)
        <a href="{{ route('show', ['id' => $publication->id]) }}" class="block rounded-lg overflow-hidden border-2 p-3 m-2 w-full max-w-md duration-300 hover:scale-105 text-white" style="background-color: #006d77;">
            <p class="font-bold text-2xl">{{ $publication->title }}</p>
            <div>{{ $publication->excerpt }}...</div>
            <p>Data utworzenia: {{ $publication->created_at->diffForHumans() }}</p>
        </a>
    @endforeach
</div>


@endsection

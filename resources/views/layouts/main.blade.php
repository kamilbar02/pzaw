<!DOCTYPE html>
<html lang="pl">

<head>
    <title> {{config('app.name')}} </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <link href="{{ asset('/resources/css/app.css') }}" rel="stylesheet">
</head>

<style type="text/tailwindcss">
    *{
        font-family: Sans-Serif;
        color: #f2f2f2;
    }
    body{
        background-color: #222831;
    }
    input, select, textarea, option{
    color: #393e46;
    }
    .footer {
        background-color: #393e46;
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        text-align: center;
    }
    ul{
        background-color: #222831;
        margin: 0%;
        padding: 0%;
        display: flex;
        position: relative;
    }
    li{
        list-style: none;
    }
    ul li:hover{
        background-color: #393e46;
    }
    .userAction{
        color: #f96d00;
    }
    </style>

<body>
    <ul>
        <!-- #TODO: highlight active reflink -->

        <li data-text="Home">
            <a class="w-full h-full relative block py-5 px-16 none text-white uppercase 
                tracking-widest overflow-hidden duration-75 hover:duration-75 z-1 hover:scale-125 hover:z-1000 "
                href="{{ route('home') }}">Strona główna </a>
        </li>
        <li data-text="About"><a class="w-full h-full relative inline-block py-5 px-16 none text-white uppercase
                tracking-widest overflow-hidden duration-75 hover:duration-75 z-1 hover:scale-125 hover:z-1000 "
                href="{{ route('about_us') }}">O nas</a></li>
        <li data-text="Publications"><a class="w-full h-full relative inline-block py-5 px-16 none text-white uppercase
                tracking-widest overflow-hidden duration-75 hover:duration-75 z-1 hover:scale-125 hover:z-1000 "
                href="{{ route('index') }}">Publikacje </a>
            </li>
        @auth
        @if(auth()->user()->id)
        <li data-text="Home">
            <a class="w-full h-full relative inline-block py-5 px-16 none text-white uppercase
                tracking-widest overflow-hidden duration-75 hover:duration-75 z-1 hover:scale-125 hover:z-1000 "
                href="{{ route('form') }}">Utwórz publikacje </a>
        </li>
        @endif
        @endauth
        <li class="userAction">
            @auth
            <form action="{{route('logout.auth')}}" method="POST" class="w-full h-full relative inline-block py-5 px-16 none uppercase
            tracking-widest overflow-hidden duration-75 hover:duration-75 z-1 hover:scale-125 hover:z-1000 ">
                @csrf
                <input type="submit" value="Wyloguj się">
            </form>
            @else
            <a class="w-full h-full relative inline-block py-5 px-16 none uppercase
            tracking-widest overflow-hidden duration-75 hover:duration-75 z-1 hover:scale-125 hover:z-1000 "
            href="{{ route('login.form') }}">Zaloguj się</a>
            @endauth
        </li>
    </ul>

    <x-alerts></x-alerts>
    <div style="background-color: #222831;">
        @yield('content')
    </div>
    <footer class="footer">
        <p>© 2023 Kamil Barczynski</p>
    </footer>
</body>

</html>
@extends('layouts.main')
@section('content')

<div class="max-w-md mx-auto m-4 p-6 rounded-md shadow-md" style="background-color:#393e46;">
    <form action="{{ route('login.auth') }}" method="POST" class="text-gray-700">
        @csrf
        <div class="mb-4">
            <label for="login" class="block">Email:</label>
            <input type="email" name="login" id="login" required
                   class="w-full px-3 py-2 mt-1 border rounded-md focus:outline-none focus:ring-blue-500 focus:border-blue-500">
            @error('password')
            <p class="text-red-500">{{$message}}</p>
            @enderror
        </div>
        <div class="mb-4">
            <label for="password" class="block">Hasło:</label>
            <input type="password" name="password" id="password" required
                   class="w-full px-3 py-2 mt-1 border rounded-md focus:outline-none focus:ring-blue-500 focus:border-blue-500">
        </div>
        <div class="mb-4">
            <label for="remember_me" class="inline-flex items-center">
                <input type="checkbox" name="remember_me" id="remember_me" class="mr-2">
                <span>Zapamiętaj mnie</span>
            </label>
        </div>
        <div>
            <button type="submit" class="w-full px-4 py-2 text-white bg-blue-500 rounded-md hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Zaloguj</button>
        </div>
    </form>
    <div class="mt-4 text-center">
        <a href="{{ route('register') }}" class="text-blue-500 hover:underline">Stworz nowe konto</a>
    </div>
</div>

@endsection

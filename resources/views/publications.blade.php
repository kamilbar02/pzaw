@extends('layouts.main')

@section('content')
<h1 class="text-3xl uppercase font-bold "> Publications </h1>
@foreach ($data as $item)
    <x-article
    title="{{ $item['title'] }}"
    date="{{$item['date'] }}"
    author="{{ $item['author'] }}"
    />
@endforeach
<div>
@endsection

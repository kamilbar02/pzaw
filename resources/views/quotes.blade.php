@extends('layouts.main')

@section('content')
<h1 class="text-3xl uppercase font-bold"> Postacie tego typu</h1>
<div class="pt-20 w-screen">
    <div class="mx-20 p-5 bg-amber-400 flex flex-column justify-center gap-x-24 gap-y-8 flex-wrap" >
    @foreach ($quotes as $item)
    <x-quote_card 
        quote="{{ $item['quote'] }}"
        hero="{{ $item['hero'] }}"
        role="{{ $item['role'] }}"
        img="{{ $item['image'] }}"
    />
    @endforeach
</div>
</div>
@endsection

@extends('layouts.main')

@section('content')
<div class="border-2 p-2 w-1/3 mt-5 m-auto" style="background-color: #02919e">
<h1 class="text-3xl uppercase font-bold "> About us </h1>
<p>
Did he speak to you? Would you just answer? What things? What people? A month ago, Gus was trying to kill both of us. And now, he pulls you out of the lab and employs you as... what... a, an assistant gunman? A tough guy? Does that make any sense to you? He says he sees something in you. What kind of game is he playing. Does he think you're that naïve? He can't truly think that you'd forget... let alone Gale, let alone Victor... and all the horror that goes along with all of that. 
It's enough. This is still the best way. You go after him with a gun, you'll never get out of it alive. But with this... you slip it into his food or drink, there shouldn't be any taste or smell... thirty-six hours later... poof. A man his age, working as hard as he does... no one will be surprised. Mike can have his suspicions, but that's all they'll be. Please, one homicidal maniac at a time. 
Look, I'll give you Jesse Pinkman, OK? Like you said, he's the problem, he's always been the problem and without him, we would... and he's in town, alright? He's not in Virginia or wherever the hell you're looking for him. He's right here in Albuquerque and I can take you to him, I'll take you right to him. What do you say? 
It's complicated and I don't wish to discuss it. It's none of your concern. You know what, let's just say that I have a hell of a lot more on my mind, right now, than thinking about buying a damn car wash. Okay? So if you could just... please. 
I gotta pay taxes? What's up with that? That's messed up. That's Kafkaesque.
- Jesse Pinkman
Somewhat Free Advice
- From Saul Goodman: Your Future Lawyer -
Tip:
Don't get caught! That solves about 90% of the problem right away.
Neat Links
Cat Dressed as Walter White
Walter White in Rubik's Cubes
This is all for the love of Breaking Bad on AMC
Make a Real Donation | Better Call Saul!
</p>
</div>
@endsection
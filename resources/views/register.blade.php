@extends('layouts.main')
@section('content')

<div class="max-w-md mx-auto m-4 p-6 rounded-md shadow-md" style="background-color:#393e46;">
    <form action="{{ route('register.store') }}" method="POST" class="text-gray-700">
        @csrf
        <div class="mb-4">
            <label for="name" class="block">Nazwa użytkownika:</label>
            <input type="text" name="name" id="name" required
                   class="w-full px-3 py-2 mt-1 border rounded-md focus:outline-none focus:ring-blue-500 focus:border-blue-500">
        </div>
        <div class="mb-4">
            <label for="email" class="block">Email:</label>
            <input type="email" name="email" id="email" required
                   class="w-full px-3 py-2 mt-1 border rounded-md focus:outline-none focus:ring-blue-500 focus:border-blue-500">
        </div>
        <div class="mb-4">
            <label for="password" class="block">Hasło:</label>
            <input type="password" name="password" id="password" required
                   class="w-full px-3 py-2 mt-1 border rounded-md focus:outline-none focus:ring-blue-500 focus:border-blue-500">
        </div>
        <div class="mb-4">
            <label for="password_confirmation" class="block">Potwierdz hasło:</label>
            <input type="password" name="password_confirmation" id="password_confirmation" required
                   class="w-full px-3 py-2 mt-1 border rounded-md focus:outline-none focus:ring-blue-500 focus:border-blue-500">
        </div>
        <div>
            <button type="submit"
                    class="w-full px-4 py-2 text-white bg-blue-500 rounded-md hover:bg-blue-600 focus:outline-none focus:bg-blue-600">
                Utwórz konto
            </button>
            <div class="mt-4 text-center">
                <a href="{{ route('login.form') }}" class="text-blue-500 hover:underline">Masz konto? Zaloguj się</a>
            </div>
        </div>
    </form>
</div>

@endsection

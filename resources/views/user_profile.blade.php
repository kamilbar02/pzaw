@extends('layouts.main')

@section('content')
<h1>{{ $user->name }}</h1>
<p>Email: {{ $user->email }}</p>
<p>Account created at: {{ $user->created_at }}</p>

<section>
    <h2>Opublikowane wpisy</h2>
    @foreach($user->publications as $publication)
        <a href="{{ route('show', ['id' => $publication->id]) }}">
            {{ $publication->title }}
        </a>
        <br>
    @endforeach
</section>

@endsection
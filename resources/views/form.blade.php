@extends('layouts.main')
@php
    $action = route('create');
    $title = null;
    $content = null;
    $author_id = null;

    if (! empty($publication)) {
        $action = route('publications.update', ['publication' => $publication->id]);
        $title = $publication->title;
        $content = $publication->content;
        $author_id = $publication->author_id;
    }
@endphp



@section('content')
<div class="border-2 p-2 w-1/3 mt-5 m-auto " style="background-color: #02919e">
    <h1 class="text-3xl py-5 uppercase font-bold">{{ !empty($publication) ? 'Edytuj publikację' : 'Dodaj nową publikację' }}</h1>
    <form action="{{ $action }}" class="flex flex-col" method="POST">
        @csrf
        @if (! empty($publication))
            @method('PUT')
        @endif
        <label>Tytuł</label>
        <input type="text" name="title" class="css-input" placeholder="Tytuł" value="{{ $title }}">
        @error('title')
        <p class="text-red-500 italic">{{ $message }}</p>
        @enderror
        <label>Treść</label>
        <textarea  name="content" class="css-input" placeholder="Treść">{{ $content }}</textarea>
        @error('content')
        <p class="text-red-500 italic ">{{ $message }}</p>
        @enderror
        <label>ID Autora</label>
        <select name="author_id" class="css-input">
            <option value="">--Wybierz autora--</option>
            @foreach($users as $user)
            <option value="{{ $user->id }}" @selected($author_id = $user->id)>{{ $user->name }}</option>
            @endforeach
        </select>
        @error('author_id')
        <p class="text-red-500 italic">{{ $message }}</p>
        @enderror
        <button type="submit" class="bg-blue-800 w-2/5 p-4 m-2 rounded"> Zapisz </button>
    </form>
    @endsection

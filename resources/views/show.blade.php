@extends('layouts.main')

@section('content')
<div class="w-screen flex justify-center">
    <div class="border-2 p-2  w-3/4 mt-10" style="background-color: #006d77;">
        <div class="flex">
        @auth
        @if($publication->author_id==auth()->user()->id)
            <div class="bg-blue-800 w-min p-4 m-4 rounded">
                <p class="w-full h-full">
                    <a href="{{ route('publications.edit', ['publication' => $publication->id]) }}">Edytuj</a>
                </p>
            </div>
        @endif
        @endauth

        @auth
        @if($publication->author_id==auth()->user()->id)
            <div class="bg-red-800 w-min p-4 m-4 rounded">
                <form action="{{ route('publications.destroy', ['publication' => $publication->id]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Usuń</button>
                </form>
            </div>
            @endif
        @endauth
        </div>
        <h1 class="text-3xl py-5 uppercase font-bold">{{$publication['title']}}</h1>
        <p class="py-1">{{ $publication['content'] }}</p>
        <p class="py-1">
            @if ($publication->author->deleted_at)
            <s>{{ $publication->author->name }}</s>
            @else
            {{ $publication->author->name }}
            @endif
        </p>
        <p class="py-1">Data utworzenia: {{ $publication['created_at'] -> diffForHumans() }}</p>
        <div id="comments">
        @auth
        @if(auth()->user()->id)
            <form action="{{ route('comments.store', $publication->id) }}" method="post">
                @csrf<div>
                    <textarea name="content" id="content" rows="2" cols="25" placeholder="Wpisz swój komentarz"
                        class="text-black"></textarea><br>
                    <button type="submit">Dodaj komentarz</button>
                    @error('password')
                    <p>{{$message}}</p>
                    @enderror
            </form> <br>
        @endif
        @endauth
        <p>Komentarze:</p>
            @foreach($comments as $comment)
            <div style="display: flex;">

            <p class="py-1">{{$comment -> text}} </p>
            @auth
            @if($comment->author_id==auth()->user()->id)
            <form action="{{ route('comments.delete', [$comment]) }}" method="POST">
                @csrf
                @method('DELETE')
                <button>Usun wpis</button>
            </form>
            @endif
            @endauth
            </div>
            @endforeach

        </div>
    </div>
</div>
@endsection
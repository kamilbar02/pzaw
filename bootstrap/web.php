<?php

use Illuminate\Support\Facades\Route;
use App\Models\Publication;
use App\Http\Controllers\PublicationController;
use App\Http\Controllers\SiteController;

// $publication = new Publication([
//     'title' => 'Sensacja! Riot usunął popularną postać Yumi z gry League of Legends!',
//     'content' => '',
//     'author' => 'Jan Kowalski'
// ]);

// $publications = [
//     new Publication([
//         'title'=> 'Ubisoft zaczal interesowac sie swoimi fanami!!',
//         'content'=> 'Rzecz niespotykania, zarzad Francuskiej firmy postanowil zrobic to na co wiele osob czekalo od lat.',
//         'author'=> 'Czeslaw michniewaz'
//     ]),
//     new Publication([
//         'title'=> 'KTOS POWIEDZIAL ZE DEBBUGOWANIE JEST PROSTE',
//         'content'=> 'WLASNIE SZUKALEM BLEDU PRZEZ POL GODZINY A OKAZALO SIE ZE ZLA NAZWA FUNKCJI BYLA PROBLEMEM ĄĄĄĄĄĄĄĄ',
//          'author'=> 'Piotr skowyrski'
//     ]),
//     new Publication([
//         'title'=> 'FORTNITE UPADA',
//         'content'=> 'ale clickbait heheeh',
//         'author'=> 'John paul'
//     ]),
//     // Dodaj kolejne publikacje w podobny sposób
// ];


// $publication = Publication::latest('created_at')->first();
// $publications = Publication::all()->sortByDesc('created_at') ;



//Podstawowy adres
// Route::get('/', function () use ($publication) {
//     return view('home',
//     ['date' => now(), 'test_int' => 2, 'publication' => $publication,]);
// }) -> name('home') ;

// //Praca na lekcji 4
// Route::get('/new/publications', function () use ($publications, $publication) {
//     return view('index', ['publications' => $publications], ['publication' => $publication]);
// }) -> name('index');


// Route::get('/new/publications/{id}', function ($id) use ($publications) {
//     return view('show', ['publications' => $publications], ['id' => $id]);
// }) -> name('show');


Route::get('/', [SiteController::class, 'latest']) -> name('home');
Route::get('/about_us', [SiteController::class, 'about_us']) -> name('about_us');
Route::get('/publications', [SiteController::class, 'publications']) -> name('publications');
Route::get('/quotes', [SiteController::class, 'quotes']) -> name('quotes');

Route::get('new/publications', [PublicationController::class, 'index'])->name('index');
Route::get('/new/publications/{id}', [PublicationController::class, 'show']) -> name('show');

//NIZEJ JEST SYF KTORY NIE JEST NA STRONIE!!!


// Route::get('greetings/{name?}', function ($name = null) {
//     if ($name == null) {
//         echo "Hejka nieznany mi czlowieku";
//     } else {
//         echo "Witaj $name jak mija ci dzien?";
//     }
// })->whereAlpha('name');


// Route::get('admin-panel/{code}', function ($code) {
//     abort_if($code != 'test123', 401, 'Podano nieprawidłowy kod dostępu.');
//     echo "Dostęp przyznany.";
// })->whereAlphaNumeric('code');
//ZADANIE 2 ---------------------------------------------------------------------
//PREFIX CZYLI PRZED KAZDYM URL TZREBA AUTOMATYCZNIE DOPISAC <zawartosc_prefix>
// Route::prefix('calculator')->group(function () {
//     Route::get('/add/{first}/{second}', function ($first, $second) {
//         $sum = $first + $second;
//         echo $sum;
//     })->whereNumber('first' and 'second');
//     Route::get('/subtract/{first}/{second}', function ($first, $second) {
//         $sum = $first - $second;
//         echo $sum;
//     })->whereNumber('first' and 'second');
//     Route::get('/multiply/{first}/{second}', function ($first, $second) {
//         $sum = $first * $second;
//         echo $sum;
//     })->whereNumber('first' and 'second');
//     Route::get('/division/{first}/{second}', function ($first, $second) {
//         $sum = $first / $second;
//         echo $sum;
//     })->whereNumber('first' and 'second');
//     Route::get('/power/{first}/{second}', function ($first, $second) {
//         for ($i = 1; $i < $second; $i++) {
//             $first *= $first;
//         }
//         echo $first;
//     })->whereNumber(['first', 'second']);
//     Route::get('/root/{first}', function ($first) {
//         $root = sqrt($first);
//         echo $root;
//     })->whereNumber('first');
// });
// //odwracanie tekstu
// Route::get('/reverse/{text}', function (string $text) {
//     $length = strlen($text);
//     $text = str_replace('_', ' ', $text);
//     for ($i = $length - 1; $i != -1; $i--) {
//         echo $text[$i];
//     }
// });
// //tablica
// $quotes = [
//     1 => [
//         'quote' => '“C`mon, I`m not here to f*ck spiders.”',
//         'hero' => 'mozzie',
//     ],
//     2 => [
//         'quote' => 'F*okin` Laser Sights',
//         'hero' => 'thatcher',
//     ],
//     3 => [
//         'quote' => 'The only thing I feel when I pull the trigger is recoil.',
//         'hero' => 'nokk',
//     ],
//     4 => [
//         'quote' => 'Acceptance of mediocrity is the first step toward failure.',
//         'hero' => 'kali',
//     ],
// ];
// //WYPISYWANIE poszczegolnych tekstow podstaci pod $id
// Route::get('quotes/{id?}', function($id = null) use ($quotes){
//     if($id == null){

//         $id = rand(1, 4);
//     }else{
//     if(!array_key_exists($id, $quotes))
//     {
//         abort(404);
//     }
// }
//     echo "Under id " . $id . " is " . $quotes[$id]['quote'] ;
// });

// Route::get('quotes/{id}/hero', function($id) use ($quotes) {
//     if(!array_key_exists($id, $quotes))
//     {
//         abort(404);
//     }
//     echo "Under id " . $id . " is " . $quotes[$id]['quote'] . "said by: " . $quotes[$id]['hero'];
// //wpisanie id oraz bohatera skutuje w "good work" inaczej "you stoopid" XD
// });
// Route::get('quotes/{id}/guess/{hero}', function($id, $hero) use ($quotes){
//     //zmiana string na caly dolny tekst
//     $changedhero = strtolower($hero);
//     if($changedhero == $quotes[$id]['hero'] ){
//         echo "good work <br>";
//     }else{
//         echo" Wrong work <br>";
//     }
// });
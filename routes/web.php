<?php


use Illuminate\Support\Facades\Route;
use App\Models\Publication;
use App\Http\Controllers\PublicationController;
use App\Http\Controllers\SiteController;
use App\Models\User;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use Illuminate\Support\Facades\Gate;

Route::get('/', [SiteController::class, 'latest']) -> name('home');

Route::get('/about_us', [SiteController::class, 'about_us']) -> name('about_us');

Route::get('/publications', [SiteController::class, 'publications']) -> name('publications');

Route::get('/quotes', [SiteController::class, 'quotes']) -> name('quotes');

Route::get('new/publications', [PublicationController::class, 'index'])->name('index');

Route::get('/new/publications/{id}', [PublicationController::class, 'show']) -> name('show');

Route::get('/forms', function(){
      $users = User::all();
      return view('form', ['users' => $users]);         
}) -> name('form');

Route::post('/form/create', [PublicationController::class, 'store']) -> name('create');

Route::get('/users/{id}', function ($id) {
    $user = User::find($id);
    if (!$user) {abort(404);}
    return view('user_profile', compact('user'));
});
Route::get('post/{publication}/edit',
 [PublicationController::class, 'edit'])->name('publications.edit');
 
Route::put('post/{publication}',
 [PublicationController::class, 'update'])->name('publications.update');
 
Route::delete('publications/{publication}',
    [PublicationController::class, 'destroy'])->name('publications.destroy');

Route::get('register', [UserController::class, 'register'])->name('register'); #TODO: make button on navbar to sign in
Route::post('/store', [UserController::class, 'store'])->name('register.store');

Route::get('/login', [AuthController::class, 'form'])->name('login.form');

Route::post('login', [AuthController::class, 'login'])->name('login.auth');
Route::post('logout', [AuthController::class, 'logout'])->name('logout.auth');

Route::post('comments/store/{id}', [CommentController::class,'store'])->name('comments.store');
Route::delete('comments/{comment}' ,[CommentController::class,'destroy'])->name('comments.delete');
//tymczasowo
Route::get('admin', function () {
    if (Gate::denies('admin-access')) {
        abort(403);
    }

    echo 'Panel administratora';
})->name('admin-panel');
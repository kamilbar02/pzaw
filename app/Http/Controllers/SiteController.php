<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Publication;

class SiteController extends Controller
{
    public function latest()
    {
            //show newest publication
            $publication = Publication::latest('created_at')->first();
            return view('home', ['post' => $publication, 'date' => date("Y-m-d H:i:s")]);            
    }
    public function about_us()
    {
            return view('about_us');            
    }
    public function publications()
    {
        $data = [
            1 => [
                'title' => 'Chce zrobic blog o asg',
                'text' => 'Dog doin great',
                'author' => 'Random guy',
                'date' => '10.10.2010',
            ],
            2 => [
                'title' => 'Dlaczego o to spryciarz to akurat spryciarz',
                'text' => 'Poniewaz kopnal z "czuba" w jakiego typka',
                'author' => 'Operator Kamery',
                'date' => '21.02.2002',
            ],
        ];
            return view('publications', ['data' => $data]);            
    }
    public function quotes()
    {
        $heroes = [
            1 => [
                'quote' => 'You were a boulder... I am a mountain.',
                'hero' => 'Sage',
                'role' => 'Sentinel',
                'image' => 'https://static.wikia.nocookie.net/valorant/images/7/74/Sage_icon.png',
            ],
            2 => [
                'quote' => 'Racing to the spike side!',
                'hero' => 'Jett',
                'role' => 'Duelist',
                'image' => 'https://static.wikia.nocookie.net/valorant/images/3/35/Jett_icon.png',
            ],
            3 => [
                'quote' => 'Call me tech support again.',
                'hero' => 'Killjoy',
                'role' => 'Sentinel',
                'image' => 'https://static.wikia.nocookie.net/valorant/images/1/15/Killjoy_icon.png',
            ],
            4 => [
                'quote' => 'One of my cameras is broken!... oh wait... it\'s fine.',
                'hero' => 'Cypher',
                'role' => 'Sentinel',
                'image' => 'https://static.wikia.nocookie.net/valorant/images/8/88/Cypher_icon.png',
            ],
            5 => [
                'quote' => 'I am the beggining. I am the end.',
                'hero' => 'Omen',
                'role' => 'Controller',
                'image' => 'https://static.wikia.nocookie.net/valorant/images/b/b0/Omen_icon.png',
            ],
        ];
            return view('quotes', ['quotes' => $heroes]);            
    }
}

<?php


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePublicationRequest;
use App\Http\Requests\UpdatePublicationRequest;
use App\Models\Publication;
use App\Models\User;

class PublicationController extends Controller
{


    public function index()
        {
            //show every publication in db
            $publications = Publication::all()->sortByDesc('created_at');
            return view('index', ['publications' => $publications]);
        }
    public function show(int $id)
    {
            //show chosen publication
            $publication = Publication::where(['id' => $id])->first();
            //{{ route('show', ['id' => $id]) }}
            if (empty($publication)) {
                abort(404);
            }
            return view('show', ['publication' => $publication, 'comments' => $publication->comments]); 
    }
    public function store(StorePublicationRequest $request)
    {
        //sprawdz dane i przypisz
        $validatedData = $request->validated();
        $newPublication = new Publication($validatedData);
        $newPublication->save();
        return redirect()->route('show', [$newPublication])->with('success', 'Akcja pomyślnie wykonana');
    }
    public function edit(Publication $publication){
        $this->authorize('update', $publication);
        return view('form', [
            'publication' => $publication,
            'users' => User::all()
        ]);
    }
    public function update(UpdatePublicationRequest $request, Publication $publication)
    {
        $this->authorize('update', $publication);
        $data = $request->validated();
        $publication->fill($data);
        $publication->save();
        return redirect()->route('show', ['id' => $publication->id])->with('success', 'Akcja pomyślnie wykonana');

    }
    public function destroy(Publication $publication)
    {
        $this->authorize('update', $publication);
        $publication->comments()->delete();
        $publication->delete();
        return redirect()->route('index')->with('success', 'Publikacja została usunięta');
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
   public function register(){
        return view('register');
    }
    public function store(UserRequest $request){

        $data = $request->validated();

        $data['password'] = bcrypt($data['password']);
        $newUser = new User($data);


        $newUser->save();

        return redirect()->route('home')->with('success', 'Akcja pomyślnie wykonana');
    }
}
?>
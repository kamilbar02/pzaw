<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePublicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
                'title' => ['required', 'string', 'min:3', 'max:50'],
                'content' => ['required', 'string', 'min:3', 'max:1500'],
                'author_id' => ['required', 'exists:users,id'],
        ];
    }
    public function messages(): array
{
   return [
       'title.required' => 'Wpisz tekst XD',
       'title.string' => 'Wpisz tekst XD',
       'title.min' => 'Za mało literek',
       'title.max' => 'Za duzo literek',
       'content.required' => 'Wpisz tekst XD',
       'content.string' => 'Wpisz tekst XD',
       'content.min' => 'Za mało literek',
       'content.max' => 'Za duzo literek',
       'author_id.required' => 'Wpisz ID',
       'author_id.exists' => 'Nie ma takiego id',
   ];


}
}

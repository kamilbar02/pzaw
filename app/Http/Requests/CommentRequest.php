<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'content' => ['required', 'string', 'min:3','max:512'],
        ];
    }
    public function messages()
    {
        return[
            'content.min' => 'Pole treści musi mieć co najmniej :min znaków.',
            'content.max' => 'Pole treści nie może mieć więcej niż :max znaków.',
        ];
    }
}
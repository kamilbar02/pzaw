<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePublicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
                'title' => ['required', 'string', 'min:3', 'max:50'],
                'content' => ['required', 'string', 'min:3', 'max:1500'],
                'author_id' => ['required', 'exists:users,id'],
        ];
    }
    public function messages(): array
{
   return [
       'title.required' => 'Tytul nie moze byc pusty',
       'title.string' => 'Blad',
       'title.min' => 'Tytul musi miec minimum 3 litery',
       'title.max' => 'Tytul musi miec max 50 liter',
       'content.required' => 'Zawartosc nie moze byc pusta',
       'content.string' => 'Wpisz tekst XD',
       'content.min' => 'Zawartosc musi miec minimum 3 litery',
       'content.max' => 'Zawartosc musi miec max 1500 liter',
       'author_id.required' => 'Wybierz tworce',
       'author_id.exists' => 'Nie ma takiego id',
   ];


}
}

<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Publication;


class PublicationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    
    public function update(?User $user, Publication $publication)
    {
        return false; // #TODO: Zamienić false na sensowne sprawdzenie...
    }
} 

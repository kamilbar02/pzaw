<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\User;

/**
 * @property int $id
 * @property int $author_id
 * @property int $publication_id
 * @property string $text
 * @property User $author
 */

 class Comment extends Model
 {
     protected $fillable = [
        'author_id',
        'text',
        'publication_id',
     ];
 
     public function author(): BelongsTo
     {
         return $this->belongsTo(User::class, 'author_id');
     }
     
 }

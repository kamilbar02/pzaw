<?php

namespace App\Models;


use App\Models\User;
use App\Models\Comment;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property string $content
 * @property string $author
 *
 * @property-read User $author
 */
class Publication extends Model
{
    protected $fillable = [
        'title',
        'content',
        'author_id'
    ];

 public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id')->withTrashed();
    }
    public function comments()
    {
        return $this->hasMany(Comment::class, 'publication_id');
    }

    protected function excerpt() : Attribute{
       
        return Attribute::make(
            get: fn () => substr($this->attributes['content'], 0, 50) ,
        ) ;

    }
    
    
}
